<?php

$word = $_POST["word"]; //guardo la palabra del usuario que viene del formulario

$text_file = $_FILES["text_file"]; //guardo el fichero que me viene del formulario que me devuelve un array

//para ver todos los campos que tiene el array a la hora de subir un fichero
foreach ($text_file as $key => $content){
    echo ("$key ==> $content");
    echo "<br>";
}

// comprobamos que se ha recibido correctamente el fichero desde html
if ($text_file["error"] !== 0) {
    echo "Ha habido un error a la hora de subir el fichero";
} else if($text_file["type"] !== "text/plain"){
    echo "Ha habido un error a la hora de subir el fichero";
} else if($text_file["size"] > 5000000){
    echo "Ha habido un error a la hora de subir el fichero, el tamaño no puede ser más grande que 5 MB";
} else {

    // primer objetivo --- guardar el fichero que me ha pasado el usuario en uploads
    // segundo obejtivo --- leer el fichero y buscar la palabra en concreto y las veces que aparece

    // 1. ruta donde lo vamos a guardar
    $path = "uploads/";

    // 2. añadiendo el fichero con la extension
    $path = $path . basename($text_file['name']); //resultado --> $path = uploads/el_quijote.txt

    // 3. guardamos el fichero en nuestro server
    $isMoved = move_uploaded_file($text_file['tmp_name'], $path);

    // 5. comprobamos si se ha subido correctamente
    if (!$isMoved) {
        echo "El fichero no se ha movido correctamente a la ubicacion: $path";
        echo "<br>";
    } else {
        echo "El fichero se ha movido correctamente a la ubicacion: $path";
        echo "<br>";

        //---------------------primer objetivo completado
        $myContent = file_get_contents($path);

        if (str_contains($myContent, $word)) {

            echo "Se ha encontado al menos una coincidencia con la palabra: $word";
            echo "<br>";

            $myContent2 = str_replace(",", " ", $myContent);
            $myContent2 = str_replace(".", " ", $myContent2);
            $myContent2 = str_replace(",", " ", $myContent2);
            $myContent2 = str_replace(";", " ", $myContent2);
            $myContent2 = str_replace('"', " ", $myContent2);
            $myContent2 = str_replace("¡", " ", $myContent2);
            $myContent2 = str_replace("!", " ", $myContent2);
            $myContent2 = str_replace("¿", " ", $myContent2);
            $myContent2 = str_replace("?", " ", $myContent2);
            $myContent2 = str_replace("(", " ", $myContent2);
            $myContent2 = str_replace(")", " ", $myContent2);

            $myContent3 = explode(" ", $myContent2);

            $wordCount = 0;

            $before = (float)microtime(); //sirve para medir el tiempo --> opcional

            foreach ($myContent3 as $content) {
                if (strtolower($content) === strtolower($word)) {
                    $wordCount++;
                }
            }

            $after = (float)microtime();

            echo "La palabra '$word' se repite $wordCount veces";
            echo "<br>";
            echo "Ha tardado " . ($after - $before) * 1000 . " milisegundos"; //le paso del microsegundos al milisegundos

            echo "<br>";

        } else {
            echo "No se ha encontrado ninguna coincidencia con la palabra: $word";
        }

        //---------------------segundo objetivo completado
    }
}





